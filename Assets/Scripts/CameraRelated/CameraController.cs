﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Vector3 rotatePoint = Vector3.zero;

	public float xSensitivity = 2.0f;
	public float ySensitivity = 2.0f;


	// Update is called once per frame
	void Update()
    {
		if (Input.GetMouseButton(0))
		{
			transform.RotateAround(rotatePoint, Vector3.up, Input.GetAxis("Mouse X") * xSensitivity);
			transform.RotateAround(rotatePoint, transform.right, Input.GetAxis("Mouse Y") * -ySensitivity);
		}
    }
}
