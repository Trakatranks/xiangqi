﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUtils
{
	public static bool IsCellEmpty(Vector2 pos, List<Piece> pieces = null)
	{
		if (pieces == null) pieces = PieceManager.Pieces;

		foreach (Piece p in pieces)
		{
			if (p.PosX == (int)pos.x && p.PosY == (int)pos.y) return false;
		}
		return true;
	}

	public static Piece GetPieceFromCoord(Vector2 pos, List<Piece> pieces = null)
	{
		if (pieces == null) pieces = PieceManager.Pieces;

		foreach (Piece p in pieces)
		{
			if (p.PosX == pos.x && p.PosY == pos.y) return p;
		}
		return null;
	}

	public static int GetTriggerIDFromCoord(Vector2 pos, Board board = null)
	{
		if (board == null) board = BoardManager.CurrentBoard;

		int cellIndex = (int)pos.x + (int)pos.y * board.boardSizeX;

		return board.cells[cellIndex].trigger.ID;
	}
}
