﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : Piece
{
	Vector2[] checkDirs =
	{
		new Vector2(0,1),
		new Vector2(0,-1),
		new Vector2(-1,0),
		new Vector2(1,0)
	};

	public Tower(Tower tower) : this(tower.PlayerID, tower.PosX, tower.PosY, tower.Value)
	{
	}

	public Tower(int playerID, int x, int y, int value) : base(playerID, x, y, value)
	{
	}

	public override List<Piece> GetFutureInstances(Board board, List<Piece> allpieces)
	{
		List<Vector2> possibleMoves = new List<Vector2>();

		for (int i = 0; i < 4; i++)
		{
			Vector2 dir = checkDirs[i];
			int counter = 0;
			bool obstacle = false;
			while (!obstacle)
			{
				counter++;
				Vector2 move = GridPos + dir * counter;

				if (move.x < 0 || move.x >= board.boardSizeX || move.y < 0 || move.y >= board.boardSizeY)
				{
					counter = 0;
					obstacle = true;
				}
				else if (!GameUtils.IsCellEmpty(move, allpieces))
				{
					counter = 0;
					obstacle = true;
					if (GameUtils.GetPieceFromCoord(move, allpieces).PlayerID != PlayerID)
					{
						possibleMoves.Add(move);
					}
				}
				else possibleMoves.Add(move);
			}
		}

		List<Piece> futureInstances = new List<Piece>();

		foreach (Vector2 move in possibleMoves)
		{
			futureInstances.Add(new Tower(PlayerID, (int)move.x, (int)move.y, Value));
		}

		return futureInstances;
	}

	public override Piece Clone()
	{
		return new Tower(this);
	}
}
