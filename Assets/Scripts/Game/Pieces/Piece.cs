﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Piece
{
	public int PlayerID = -1;
	
	public int Value = 0;

	public int PosX;
	public int PosY;

	public Vector2 GridPos { get { return new Vector2(PosX, PosY); } private set { } }

	public PieceColor Color;

	public int CellIndex {
		get { return PosX + (PosY * BoardManager.CurrentBoard.boardSizeX); }
		private set { }
	}

	public Piece(Piece piece) : this(piece.PlayerID, piece.PosX, piece.PosY, piece.Value)
	{
	}

	public Piece(int playerID, int x, int y, int value)
	{
		PlayerID = playerID;
		PosX = x;
		PosY = y;
		Value = value;
		if (playerID == 0) Color = PieceColor.WHITE;
		else Color = PieceColor.BLACK;
	}

	public virtual List<Piece> GetFutureInstances(Board board, List<Piece> allpieces)
	{
		throw new Exception("PIECE CALL : GetFutureInstances");
	}

	internal void Destroy()
	{
		throw new NotImplementedException();
	}

	public virtual Piece Clone()
	{
		throw new Exception("PIECE CALL : Clone");
	}
}

[System.Serializable]
public enum PieceType
{
	PAWN,
	CANNON,
	TOWER,
	HORSE,
	ELEPHANT,
	MINISTER,
	KING,
	NONE
}

[System.Serializable]
public enum PieceColor
{
	BLACK,
	WHITE,
	ANY
}