﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : Piece
{
	int sideID = 0;

	int boardDirection = -1;

	public Pawn(Pawn piece) : this(piece.PlayerID, piece.PosX, piece.PosY, piece.Value)
	{
	}

	public Pawn(int playerID, int x, int y, int value) : base(playerID, x, y, value)
	{
	}

	public override List<Piece> GetFutureInstances(Board board, List<Piece> allpieces)
	{
		if (Color == PieceColor.WHITE) boardDirection = 1;

		sideID = Color == PieceColor.WHITE ? 0 : 1;

		List<Vector2> possibleMoves = new List<Vector2>();

		possibleMoves.Add(new Vector2(PosX, PosY + boardDirection));

		if (GameUtils.GetTriggerIDFromCoord(GridPos) != sideID)
		{
			possibleMoves.Add(new Vector2(PosX + 1, PosY));
			possibleMoves.Add(new Vector2(PosX - 1, PosY));
		}

		for (int i = possibleMoves.Count - 1; i >= 0; i--)
		{
			Vector2 move = possibleMoves[i];

			if (move.x < 0 || move.x >= board.boardSizeX || move.y < 0 || move.y >= board.boardSizeY)
			{
				possibleMoves.RemoveAt(i);
				continue;
			}

			if (!GameUtils.IsCellEmpty(move, allpieces) && GameUtils.GetPieceFromCoord(move, allpieces).PlayerID == PlayerID)
			{
				possibleMoves.RemoveAt(i);
				continue;
			}
		}

		List<Piece> futureInstances = new List<Piece>();

		foreach (Vector2 move in possibleMoves)
		{
			futureInstances.Add(new Pawn(PlayerID, (int)move.x, (int)move.y, Value));
		}

		return futureInstances;
	}

	public override Piece Clone()
	{
		return new Pawn(this);
	}
}
