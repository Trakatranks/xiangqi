﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceController : MonoBehaviour
{
	public Piece PieceRef;

	public int Value = 0;

	public PieceType Type;

	[HideInInspector]
	public bool isHovered = false;
	[HideInInspector]
	public bool isSelected = false;

	private void Update()
	{
		Ray ray = GameManager.mouseRay;
		int pieceLayer = LayerMask.GetMask("PieceRaycasts");

		if (Physics.Raycast(ray, out RaycastHit hoverInfo, 150, pieceLayer) && hoverInfo.transform.gameObject == transform.gameObject)
		{
			if (!isHovered && !isSelected)
			{
				isHovered = true;
				EventManager.EmitOnPieceHoverIn(this);
			}
		}
		else if (isHovered || isSelected)
		{
			isHovered = false;
			EventManager.EmitOnPieceHoverOut(this);
		}

		if (Input.GetMouseButtonDown(0))
		{
			if(Physics.Raycast(ray, out RaycastHit hitInfo))
			{
				if(hitInfo.transform.gameObject == transform.gameObject)
				{
					EventManager.EmitOnPieceClick(this);
				}
			}
		}
	}

	public void InitPiece(int x, int y, int playerID)
	{
		switch (Type)
		{
			case PieceType.PAWN:
				PieceRef = new Pawn(playerID, x, y, Value);
				break;
			case PieceType.CANNON:
				PieceRef = new Cannon(playerID, x, y, Value);
				break;
			case PieceType.TOWER:
				PieceRef = new Tower(playerID, x, y, Value);
				break;
			case PieceType.HORSE:
				PieceRef = new Horse(playerID, x, y, Value);
				break;
			case PieceType.ELEPHANT:
				PieceRef = new Elephant(playerID, x, y, Value);
				break;
			case PieceType.MINISTER:
				PieceRef = new Minister(playerID, x, y, Value);
				break;
			case PieceType.KING:
				PieceRef = new King(playerID, x, y, Value);
				break;
			case PieceType.NONE:
				break;
			default:
				break;
		}

		PieceRef.Value = Value;

		PieceManager.Pieces.Add(PieceRef);

		if (playerID == 0) GetComponent<MeshRenderer>().material.SetColor("_PieceColor", Color.red);
	}
}

[System.Serializable]
public enum KanjiStyle
{
	SIMPLE,
	COMPLEX
}