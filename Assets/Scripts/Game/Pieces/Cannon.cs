﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : Piece
{
	Vector2[] checkDirs =
	{
		new Vector2(0,1),
		new Vector2(0,-1),
		new Vector2(-1,0),
		new Vector2(1,0)
	};


	public Cannon(Cannon cannon) : this(cannon.PlayerID, cannon.PosX, cannon.PosY, cannon.Value)
	{
	}
	public Cannon(int playerID, int x, int y, int value) : base(playerID, x, y, value)
	{
	}

	public override List<Piece> GetFutureInstances(Board board, List<Piece> allpieces)
	{
		List<Vector2> possibleMoves = new List<Vector2>();

		for (int i = 0; i < 4; i++)
		{
			Vector2 dir = checkDirs[i];
			int counter = 0;
			while (true)
			{
				counter++;
				Vector2 move = GridPos + dir * counter;

				if (move.x < 0 || move.x >= board.boardSizeX || move.y < 0 || move.y >= board.boardSizeY)
				{
					counter = 0;
					break;
				}
				else if (!GameUtils.IsCellEmpty(move, allpieces))
				{
					int next = 0;
					while (true)
					{
						next++;
						Vector2 checkMove = move + dir * next;
						if (checkMove.x < 0 || checkMove.x >= board.boardSizeX || checkMove.y < 0 || checkMove.y >= board.boardSizeY)
						{
							break;
						}
						else
						{
							if (!GameUtils.IsCellEmpty(checkMove, allpieces))
							{
								if (GameUtils.GetPieceFromCoord(checkMove, allpieces).PlayerID != PlayerID)
								{
									possibleMoves.Add(checkMove);
								}
								break;
							}
						}
					}
					break;
				}
				else possibleMoves.Add(move);
			}
		}

		List<Piece> futureInstances = new List<Piece>();

		foreach (Vector2 move in possibleMoves)
		{
			futureInstances.Add(new Cannon(PlayerID, (int)move.x, (int)move.y, Value));
		}

		return futureInstances;
	}

	public override Piece Clone()
	{
		return new Cannon(this);
	}
}
