﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horse : Piece
{
	Vector2[] checkDirs =
	{
		new Vector2(0,1),
		new Vector2(0,-1),
		new Vector2(-1,0),
		new Vector2(1,0)
	};
	
	public Horse(Horse horse) : this(horse.PlayerID, horse.PosX, horse.PosY, horse.Value)
	{
	}
	public Horse(int playerID, int x, int y, int value) : base(playerID, x, y, value)
	{
	}

	public override List<Piece> GetFutureInstances(Board board, List<Piece> allpieces)
	{
		List<Vector2> possibleMoves = new List<Vector2>();

		for (int i = 0; i < 4; i++)
		{
			Vector2 cellpos = checkDirs[i] + GridPos;

			int cellPosIndex = (int)(cellpos.x + cellpos.y * board.boardSizeX);

			if (cellPosIndex < 0 || cellPosIndex >= board.boardSizeX * board.boardSizeY) continue;

			if (GameUtils.IsCellEmpty(cellpos, allpieces))
			{
				Vector2 ds = checkDirs[i] * checkDirs[i];
				Vector2 dsom = Vector2.one - ds;
				possibleMoves.Add(GridPos + (checkDirs[i] * 2) + dsom);
				possibleMoves.Add(GridPos + (checkDirs[i] * 2) - dsom);
			}
		}

		for (int i = possibleMoves.Count - 1; i >= 0; i--)
		{
			Vector2 move = possibleMoves[i];

			if (move.x < 0 || move.x >= board.boardSizeX || move.y < 0 || move.y >= board.boardSizeY)
			{
				possibleMoves.RemoveAt(i);
				continue;
			}

			if (!GameUtils.IsCellEmpty(move, allpieces))
			{
				if (GameUtils.GetPieceFromCoord(move, allpieces).PlayerID == PlayerID)
				{
					possibleMoves.RemoveAt(i);
					continue;
				}
			}
		}

		List<Piece> futureInstances = new List<Piece>();

		foreach (Vector2 move in possibleMoves)
		{
			futureInstances.Add(new Horse(PlayerID, (int)move.x, (int)move.y, Value));
		}

		return futureInstances;
	}

	public override Piece Clone()
	{
		return new Horse(this);
	}
}
