﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Elephant : Piece
{
	int sideID = 0;

	Vector2[] checkDirs =
	{
		new Vector2(1,1),
		new Vector2(1,-1),
		new Vector2(-1,1),
		new Vector2(-1,-1)
	};


	public Elephant(Elephant elephant) : this(elephant.PlayerID, elephant.PosX, elephant.PosY, elephant.Value)
	{
	}
	public Elephant(int playerID, int x, int y, int value) : base(playerID, x, y, value)
	{
	}

	public override List<Piece> GetFutureInstances(Board board, List<Piece> allpieces)
	{
		sideID = Color == PieceColor.WHITE ? 0 : 1;

		List<Vector2> possibleMoves = new List<Vector2>();

		for (int i = 0; i < 4; i++)
		{
			Vector2 cellpos = checkDirs[i] + GridPos;

			int cellPosIndex = (int)(cellpos.x + cellpos.y * board.boardSizeX);

			if (cellPosIndex < 0 || cellPosIndex >= board.boardSizeX * board.boardSizeY) continue;

			if (GameUtils.IsCellEmpty(cellpos, allpieces))
			{
				possibleMoves.Add(GridPos + (checkDirs[i] * 2));
			}
		}

		for (int i = possibleMoves.Count - 1; i >= 0; i--)
		{
			Vector2 move = possibleMoves[i];

			if (move.x < 0 || move.x >= board.boardSizeX || move.y < 0 || move.y >= board.boardSizeY)
			{
				possibleMoves.RemoveAt(i);
				continue;
			}

			bool goodMoveCellId = (GameUtils.GetTriggerIDFromCoord(move) != sideID && GameUtils.GetTriggerIDFromCoord(move) != sideID + 2);
			bool moveCellEmpty = (!GameUtils.IsCellEmpty(move, allpieces) && GameUtils.GetPieceFromCoord(move, allpieces).PlayerID == PlayerID);

			if (goodMoveCellId || moveCellEmpty)
			{
				possibleMoves.RemoveAt(i);
			}
		}

		List<Piece> futureInstances = new List<Piece>();

		foreach (Vector2 move in possibleMoves)
		{
			futureInstances.Add(new Elephant(PlayerID, (int)move.x, (int)move.y, Value));
		}

		return futureInstances;
	}

	public override Piece Clone()
	{
		return new Elephant(this);
	}
}
