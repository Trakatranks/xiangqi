﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minister : Piece
{
	int castleID = -1;


	public Minister(Minister minister) : this(minister.PlayerID, minister.PosX, minister.PosY, minister.Value)
	{
	}
	public Minister(int playerID, int x, int y, int value) : base(playerID, x, y, value)
	{
	}

	public override List<Piece> GetFutureInstances(Board board, List<Piece> allpieces)
	{
		List<Vector2> possibleMoves = new List<Vector2>();

		if (Color == PieceColor.WHITE) castleID = 2;
		else castleID = 3;

		possibleMoves.Add(new Vector2(PosX + 1, PosY + 1));
		possibleMoves.Add(new Vector2(PosX - 1, PosY + 1));
		possibleMoves.Add(new Vector2(PosX + 1, PosY - 1));
		possibleMoves.Add(new Vector2(PosX - 1, PosY - 1));

		for (int i = possibleMoves.Count - 1; i >= 0; i--)
		{
			Vector2 move = possibleMoves[i];

			if (move.x < 0 || move.x >= board.boardSizeX || move.y < 0 || move.y >= board.boardSizeY)
			{
				possibleMoves.RemoveAt(i);
				continue;
			}
			
			if (GameUtils.GetTriggerIDFromCoord(move) != castleID)
			{
				possibleMoves.RemoveAt(i);
				continue;
			}
			else if (!GameUtils.IsCellEmpty(move, allpieces))
			{
				if (GameUtils.GetPieceFromCoord(move, allpieces).PlayerID == PlayerID)
				{
					possibleMoves.RemoveAt(i);
					continue;
				}
			}
		}

		List<Piece> futureInstances = new List<Piece>();

		foreach (Vector2 move in possibleMoves)
		{
			futureInstances.Add(new Minister(PlayerID, (int)move.x, (int)move.y, Value));
		}

		return futureInstances;
	}

	public override Piece Clone()
	{
		return new Minister(this);
	}
}
