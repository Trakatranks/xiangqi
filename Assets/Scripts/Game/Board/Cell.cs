﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cell : MonoBehaviour
{
	public bool isWalkable = false;

	public int zoneType = 0;

	//public PieceData pieceData;

	public Trigger trigger;

	public int gridPosX = 0;
	public int gridPosY = 0;

	private void OnDrawGizmos()
	{
		if (GameUtils.IsCellEmpty(GetGridPos(), PieceManager.Pieces))
		{
			Gizmos.color = Color.green;
		}
		else Gizmos.color = Color.red;

		Gizmos.DrawCube(transform.position, Vector3.one * (Mathf.Cos(Time.time * 3)/2+1));
	}
	
	public Vector2 GetGridPos()
	{
		return new Vector2(gridPosX, gridPosY);
	}
	

	public void InitCellFromData(CellData data)
	{
		isWalkable = data.isWalkable;
		zoneType = data.zoneType;

		gridPosX = data.gridPosX;
		gridPosY = data.gridPosY;

		trigger = data.trigger;
	}

	public void DestroyCell()
	{
		Destroy(gameObject);
	}
}
