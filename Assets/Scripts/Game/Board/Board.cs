﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Board : MonoBehaviour
{
	public int boardSizeX = 1;
	public int boardSizeY = 1;
	public List<Cell> cells;

	public GameObject cellPrefab;

	[Header("Pieces")]
	public GameObject pawnPrefab;
	public GameObject cannonPrefab;
	public GameObject towerPrefab;
	public GameObject horsePrefab;
	public GameObject elephantPrefab;
	public GameObject ministerPrefab;
	public GameObject kingPrefab;

	[Header("UI")]
	[SerializeField]
	protected float _guiSphereSize = .1f;

	[Header("Board Config")]
	[Space()]
	[SerializeField]
	public float _rowSpace = 1.0f;
	[SerializeField]
	public float _colSpace = 1.0f;
	[SerializeField]
	public float _riverSpace = 1.5f;

	//GIZMOS
	private void OnDrawGizmos()
	{
		float count = 0;
		float total = boardSizeX * boardSizeY;
		for (int y = 0; y < boardSizeY; y++)
		{
			for (int x = 0; x < boardSizeX; x++)
			{
				Gizmos.color = new Color(count/total, count / total, count / total);
				Gizmos.DrawSphere(GetGridWorldPos(x, y), _guiSphereSize);
				count++;
			}
		}
	}

	public Cell GetCellFromCoord(int x, int y)
	{
		int index = x + y * boardSizeX;
		if (index > 0 && index < boardSizeX * boardSizeY)
		{
			return cells[index];
		}
		else return null;
	}

	public Cell GetCellFromCoord(Vector2 vec2)
	{
		int index = (int)(vec2.x + vec2.y * boardSizeX);
		if (index >= 0 && index < boardSizeX * boardSizeY)
		{
			return cells[index];
		}
		else return null;
	}

	public Vector3 GetGridWorldPos(int x, int y)
	{
		float gridSizeX = (boardSizeX - 1) * _colSpace;
		float gridSizeY = (boardSizeY - 1) * _rowSpace;

		Vector3 worldPos = transform.position + transform.right * (x * _colSpace - gridSizeX / 2) + transform.forward * (y * _rowSpace - gridSizeY / 2);

		if (y <= (boardSizeY - 1) / 2) worldPos -= transform.forward * (_riverSpace / 2);
		else if (y > (boardSizeY - 1) / 2) worldPos += transform.forward * (_riverSpace / 2);

		return worldPos;
	}

	public Vector3 GetGridWorldPos(Vector2 pos)
	{
		float gridSizeX = (boardSizeX - 1) * _colSpace;
		float gridSizeY = (boardSizeY - 1) * _rowSpace;

		Vector3 worldPos = transform.position + transform.right * (pos.x * _colSpace - gridSizeX / 2) + transform.forward * (pos.y * _rowSpace - gridSizeY / 2);

		if (pos.y <= (boardSizeY - 1) / 2) worldPos -= transform.forward * (_riverSpace / 2);
		else if (pos.y > (boardSizeY - 1) / 2) worldPos += transform.forward * (_riverSpace / 2);

		return worldPos;
	}

	public void ResetCells()
	{
		for (int i = cells.Count - 1; i >= 0; i--)
		{
			cells[i].DestroyCell();
		}
		cells.Clear();
	}

	public void GenerateFromBoardData(BoardData boardData)
	{
		if(cells.Count > 0) ResetCells();

		boardSizeX = boardData.boardSizeX;
		boardSizeY = boardData.boardSizeY;

		for (int y = 0; y < boardSizeY; y++)
		{
			for (int x = 0; x < boardSizeX; x++)
			{
				CellData currentCellData = boardData.cells[x + (y * boardSizeX)];

				GameObject go = Instantiate(cellPrefab, GetGridWorldPos(x, y), Quaternion.identity, transform);
				Cell cell = go.GetComponent<Cell>();

				cell.InitCellFromData(currentCellData);

				cells.Add(cell);
				
				if (currentCellData.pieceData.pieceType != PieceType.NONE)
				{
					GameObject piecePrefab;
					switch (currentCellData.pieceData.pieceType)
					{
						case PieceType.PAWN:
							piecePrefab = pawnPrefab;
							break;
						case PieceType.CANNON:
							piecePrefab = cannonPrefab;
							break;
						case PieceType.TOWER:
							piecePrefab = towerPrefab;
							break;
						case PieceType.HORSE:
							piecePrefab = horsePrefab;
							break;
						case PieceType.ELEPHANT:
							piecePrefab = elephantPrefab;
							break;
						case PieceType.MINISTER:
							piecePrefab = ministerPrefab;
							break;
						case PieceType.KING:
							piecePrefab = kingPrefab;
							break;
						default:
							piecePrefab = pawnPrefab;
							break;
					}

					PieceController piece = Instantiate(piecePrefab, cell.transform.position, Quaternion.identity).GetComponent<PieceController>();

					piece.InitPiece(x, y, currentCellData.pieceData.playerID);
				}
			}
		}
	}
}
