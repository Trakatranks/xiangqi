﻿[System.Serializable]
public class PieceData
{
	public int playerID;
	public PieceType pieceType = PieceType.NONE;

	public PieceData(int pPlayerID = 0, PieceType pPieceType = PieceType.NONE)
	{
		playerID = pPlayerID;
		pieceType = pPieceType;
	}
}
