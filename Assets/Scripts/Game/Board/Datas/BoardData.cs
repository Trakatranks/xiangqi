﻿[System.Serializable]
public class BoardData
{
	public int boardSizeX = 1;
	public int boardSizeY = 1;
	public CellData[] cells;

	public BoardData(int x, int y)
	{
		boardSizeX = x;
		boardSizeY = y;
		cells = new CellData[x*y];
	}
}