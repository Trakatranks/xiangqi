﻿[System.Serializable]
public class CellData
{
	public bool isPalace = false;
	public bool isWalkable = true;

	public int zoneType = 0;

	public PieceData pieceData;
	public Trigger trigger;

	public int gridPosX = 0;
	public int gridPosY = 0;

	public CellData()
	{
		pieceData = new PieceData();
		trigger = new Trigger();
	}
}