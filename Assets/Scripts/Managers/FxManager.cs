﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxManager : MonoBehaviour
{
	//Unity variables
	[SerializeField]
	protected GameObject moveFxGO;
	[SerializeField]
	protected GameObject selectFxGO;
	[SerializeField]
	protected GameObject hoverMoveFxGO;

	protected static GameObject selectFx;
	protected static GameObject hoverMoveFx;
	protected static GameObject moveFx;
	
	protected static List<GameObject> possibleMovesFxs = new List<GameObject>();
	protected static List<GameObject> hoverMovesFxs = new List<GameObject>();

	private void Start()
	{
		selectFx = selectFxGO;
		hoverMoveFx = hoverMoveFxGO;
		moveFx = moveFxGO;
	}

	//SHOW FUNCTIONS

	public static void ShowPossibleMove(Vector3 worldPos, bool clearFxs = false)
	{
		if (clearFxs)
		{
			ClearPossibleMoves();
		}
		GameObject fx = Instantiate(moveFx, worldPos, Quaternion.identity);
		possibleMovesFxs.Add(fx);
	}

	public static void ShowHoverMove(Vector3 worldPos, bool clearFxs = false)
	{
		if (clearFxs)
		{
			ClearHoverMoves();
		}
		GameObject fx = Instantiate(hoverMoveFx, worldPos, Quaternion.identity);
		hoverMovesFxs.Add(fx);
	}

	//CLEAR FUNCTIONS

	public static void ClearPossibleMoves()
	{
		foreach (GameObject movefx in possibleMovesFxs)
		{
			GameObject fx = movefx;
			Destroy(fx);
		}
		possibleMovesFxs.Clear();
	}

	public static void ClearHoverMoves()
	{
		foreach (GameObject movefx in hoverMovesFxs)
		{
			GameObject fx = movefx;
			Destroy(fx);
		}
		hoverMovesFxs.Clear();
	}

}
