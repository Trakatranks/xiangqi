﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
	public static Board CurrentBoard;

	protected const string CLASSIC_MAP = "classicmap";
	protected const string CLASSIC_MAP2 = "test";
	protected const string CLASSIC_MAP3 = "test2";
	protected const string HORSE = "horsetest";
	protected const string MAP_1 = "map1";
	protected const string STUPID = "Stupid";
	protected const string BITE = "Bite";
	protected const string CAROLE = "essai";
	protected const string SANTEA = "TestGeoffroy";

	[SerializeField]
	protected Board boardGO;

	[Header("PieceConfig")]
	[SerializeField]
	public KanjiStyle _piecesStyle;
	
	private void Start()
	{
		CurrentBoard = boardGO;
		GenerateNewBoard();
	}
	
	public static void GenerateNewBoard()
	{
		CurrentBoard.GenerateFromBoardData(BoardSaver.LoadBoard(CLASSIC_MAP));
	}
}
