﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
	public RectTransform text;

    // Start is called before the first frame update
    void Start()
    {
		LeanTween.alphaText(text, 0, 5f);
		LeanTween.moveLocalX(text.gameObject, text.position.x + 10, 5f);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
