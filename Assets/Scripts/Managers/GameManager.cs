﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void ActionDelegate();

public class GameManager : MonoBehaviour
{
	public int result = 0;

	public static GameState currentGameState = GameState.PLAYER_TURN;

	public Camera gameCamera;

	public ActionDelegate DoAction;

	public static Ray mouseRay;

	private void Start()
	{
		ChangeStateAction();
	}

	private void Update()
	{
		mouseRay = gameCamera.ScreenPointToRay(Input.mousePosition);

		DoAction();

		if (Input.GetKeyDown(KeyCode.X))
		{
			Application.Quit();
		}
		if (Input.GetKeyDown(KeyCode.R))
		{
			PieceManager.ResetAll();
			BoardManager.GenerateNewBoard();
			FxManager.ClearPossibleMoves();
			FxManager.ClearHoverMoves();
		}
	}

	protected void ChangeStateAction()
	{
		switch (currentGameState)
		{
			case GameState.PLAYER_TURN:
				DoAction = PlayerTurnAction;
				break;
			case GameState.AI_TURN:
				DoAction = AITurnAction;
				break;
			case GameState.END:
				break;
			default:
				break;
		}
	}

	protected void PlayerTurnAction()
	{
		/*
		if (PieceManager.selectedPiece != null)
		{
			if (Input.GetMouseButtonDown(0))
			{
				RaycastHit hitInfo = new RaycastHit();

				int layer = LayerMask.GetMask("CellRaycasts");

				if (Physics.Raycast(mouseRay, out hitInfo, 150, layer))
				{
					Cell clickedCell = hitInfo.collider.GetComponent<Cell>();
					
					PieceManager.MovePiece(PieceManager.selectedPiece, clickedCell);

					PieceManager.DeselectPiece();

					FxManager.ClearPossibleMoves();

					//SetActionAi();
				}
			}
			else if (Input.GetMouseButtonDown(1))
			{
				PieceManager.DeselectPiece();

				FxManager.ClearPossibleMoves();
			}
		}
		*/
		if (Input.GetKeyDown(KeyCode.J)) SetActionAi();
	}

	protected void AITurnAction()
	{

	}

	protected void SetActionPlayer()
	{
		currentGameState = GameState.PLAYER_TURN;
		ChangeStateAction();
	}

	protected void SetActionAi()
	{
		currentGameState = GameState.AI_TURN;
		AiManager.UpdateCurrentState();
		ChangeStateAction();
	}
}

public enum GameState
{
	PLAYER_TURN,
	AI_TURN,
	END
}