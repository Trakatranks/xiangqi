﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void EventPieceClick(PieceController sender); 

public class EventManager
{
	public static event EventPieceClick OnPieceClick;
	public static event EventPieceClick OnPieceHoverIn;
	public static event EventPieceClick OnPieceHoverOut;

	public static void EmitOnPieceClick(PieceController sender)
	{
		if (GameManager.currentGameState != GameState.PLAYER_TURN) return;

		if (OnPieceClick != null)
		{
			OnPieceClick(sender);
		}
	}

	public static void EmitOnPieceHoverIn(PieceController sender)
	{
		if (OnPieceHoverIn != null)
		{
			OnPieceHoverIn(sender);
		}
	}

	public static void EmitOnPieceHoverOut(PieceController sender)
	{
		if (OnPieceHoverOut != null)
		{
			OnPieceHoverOut(sender);
		}
	}
}
