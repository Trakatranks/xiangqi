﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceManager : MonoBehaviour
{
	public static List<Piece> Pieces = new List<Piece>();

	public static List<Vector2> crntPieceMoves;

	public static Piece selectedPiece;
	public static Cell currentCell;

	private void OnEnable()
	{
		EventManager.OnPieceClick += PieceSelect;
		EventManager.OnPieceHoverIn += PieceHoverIn;
		EventManager.OnPieceHoverOut += PieceHoverOut;
	}

	/// <summary>
	/// Physically moves a Piece to a specific Cell. Returns true if a piece has been moved.
	/// </summary>
	/// <param name="piece">The piece being moved</param>
	/// <param name="cell">The cell the piece is being moved on</param>
	/// <returns></returns>
	public static bool MovePiece(Piece piece, Cell cell)
	{
		return true;
		/*
		if (crntPieceMoves.Contains(cell.GetGridPos())) //If the target cell is part of the piece move possibilities
		{
			if (!cell.IsEmpty)
			{
				if (cell.pieceData.playerID == selectedPiece.PlayerID) //if the cell is not empty but the piece on it belongs to the current player
				{
					return false;
				}
				else
				{
					DestroyPiece(FindPieceFromCell(cell));
				}
			}
			BoardManager.CurrentBoard.cells[piece.CellIndex].SetEmpty();
			
			LeanTween.move(piece.gameObject, cell.transform, 0.5f).setEaseInOutCubic();
			LeanTween.moveY(piece.gameObject, 1, .25f).setLoopPingPong(1).setEaseOutCubic();
			piece.SetGridPos(cell.gridPosX, cell.gridPosY);
			cell.SetCellData(piece);

			return true;
		}
		else return false;
		*/
	}

	/// <summary>
	/// Returns a dictionnary containing all the moves for each piece of one or either sides.
	/// </summary>
	/// <param name="pieces"></param>
	/// <param name="side"></param>
	/// <returns></returns>
	public static Dictionary<Piece, List<Piece>> CheckAllPieceMoves(List<Piece> pieces, PieceColor side = PieceColor.ANY)
	{
		Dictionary<Piece, List<Piece>> moves = new Dictionary<Piece, List<Piece>>();

		foreach (Piece piece in pieces)
		{
			if (piece.Color != side && side != PieceColor.ANY) continue;

			List<Piece> pieceMoves = piece.GetFutureInstances(BoardManager.CurrentBoard, Pieces);
			moves.Add(piece, pieceMoves);
		}
		return moves;
	}

	protected static void DestroyPiece(Piece piece)
	{
		piece.Destroy();
	}

	protected static Piece FindPieceFromCell(Cell cell)
	{
		Vector2 cellPos = cell.GetGridPos();
		foreach (Piece piece in Pieces)
		{
			if(piece.GridPos == cellPos)
				return piece;
		}
		return null;
	}

	public void PieceSelect(PieceController sender)
	{
		/*
		SelectPiece(sender);
		selectedPiece.isHovered = false;
		FxManager.ClearHoverMoves();

		currentCell = BoardManager.CurrentBoard.cells[selectedPiece.CellIndex];
		crntPieceMoves = selectedPiece.CheckPossibleMoves();
		List<Vector3> cppm = new List<Vector3>();
		if (crntPieceMoves.Count == 0) return;
		FxManager.ClearPossibleMoves();
		foreach (Vector2 move in crntPieceMoves)
		{
			Vector3 moveBoardPos = BoardManager.CurrentBoard.GetGridWorldPos((int)move.x, (int)move.y);
			cppm.Add(moveBoardPos);
			FxManager.ShowPossibleMove(moveBoardPos);
		}
		*/
	}

	private void PieceHoverIn(PieceController sender)
	{
		/*
		List<Vector2> pieceMoves = sender.CheckPossibleMoves();
		foreach (Vector2 move in pieceMoves)
		{
			Vector3 movePos = BoardManager.CurrentBoard.GetGridWorldPos(move);
			FxManager.ShowHoverMove(movePos);
		}
		*/
	}

	private void PieceHoverOut(PieceController sender)
	{
		FxManager.ClearHoverMoves();
	}

	public static void ResetAll()
	{
		for (int i = Pieces.Count - 1; i >= 0; i--)
		{
			Pieces[i].Destroy();
		}
		Pieces = new List<Piece>();
		crntPieceMoves = new List<Vector2>();
		DeselectPiece();
		currentCell = null;
	}

	public static void SelectPiece(PieceController piece)
	{
		DeselectPiece();
		//selectedPiece = piece;
		piece.isSelected = true;
	}

	public static void DeselectPiece()
	{
		if (selectedPiece == null) return;
		//selectedPiece.isSelected = false;
		selectedPiece = null;
	}

	private void OnDisable()
	{
		EventManager.OnPieceClick -= PieceSelect;
		EventManager.OnPieceHoverIn -= PieceHoverIn;
		EventManager.OnPieceHoverOut -= PieceHoverOut;
	}
}
