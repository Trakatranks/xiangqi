﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class AiManager
{
	public static PieceColor AiColor = PieceColor.BLACK;

	public static int Recursive = 0;

	static List<State> states;

	static State currentState;

	public static void UpdateCurrentState()
	{
		currentState = new State();
		currentState.pieces = new List<Piece>();

		foreach (Piece p in PieceManager.Pieces)
		{
			currentState.pieces.Add(p.Clone());
		}

		currentState.nextStates = GetNextStates(BoardManager.CurrentBoard, currentState, Recursive);

		Debug.Log(currentState.score);
		Debug.Log(currentState.nextStates.Count);

		int moveID = 0;

		foreach (State state in currentState.nextStates)
		{
			if (state.score != currentState.score) continue;
			moveID++;
			Debug.Log(moveID + " || " + state.movedPiece.Color + " " + state.movedPiece.GetType() + " a bougé en " + state.movedPiece.PosX + "/" + state.movedPiece.PosY + " sur un state a " + state.score);
		}
	}
	
	protected static List<State> GetNextStates(Board board, State parentState, int recursivity, bool isMaxTurn = true)
	{
		List<State> nextStates = new List<State>();

		foreach (Piece currentPiece in parentState.pieces) //pour chaque pièce du current state
		{
			if (isMaxTurn) //Tour de l'IA
			{
				if (currentPiece.Color != AiColor) continue; //Ignore les rouges
			}
			else //Tour du joueur
			{
				if (currentPiece.Color == AiColor) continue; //Ignore les noirs
			}

			List<Piece> futurePieceInstances = currentPiece.GetFutureInstances(board, parentState.pieces);

			foreach (Piece futurePiece in futurePieceInstances) // pour chaque mouvement de cette pièce
			{
				//Recrée un state original identique
				State newState = new State
				{
					beta = parentState.beta,
					alpha = parentState.alpha,
					parent = parentState,
					pieces = new List<Piece>()
				};

				//bouge la pièce et replace les anciennes
				foreach (Piece p in parentState.pieces)
				{
					if (p == currentPiece)
					{
						newState.pieces.Add(futurePiece);
						newState.movedPiece = futurePiece;
					}
					else {
						newState.pieces.Add(p.Clone());
					}
				}
				
				//retire la pièce éventuellement mangée
				for (int i = newState.pieces.Count - 1; i >= 0; i--)
				{
					if(newState.pieces[i].GridPos == futurePiece.GridPos && newState.pieces[i] != futurePiece)
					{
						newState.pieces.RemoveAt(i);
					}
				}

				//évalue la situation
				if(recursivity == 0)
				{
					newState.Evaluate();
				}
				else
				{
					newState.nextStates = GetNextStates(board, newState, recursivity - 1, !isMaxTurn);
				}

				nextStates.Add(newState);

				if (isMaxTurn)
				{
					if (!parentState.score.HasValue)
					{
						parentState.score = newState.score;
						parentState.beta = newState.score.Value;
					}
					else
					{
						parentState.score = Mathf.Max(parentState.score.Value, newState.score.Value);
						parentState.beta = Mathf.Max(parentState.beta, newState.score.Value);
					}

					if (parentState.score.Value < parentState.alpha)
					{
						Debug.Log("MINMAXPRUNING ++");

						return nextStates;
					}
				}
				else
				{
					if (!parentState.score.HasValue)
					{
						parentState.score = newState.score.Value;
						parentState.alpha = newState.score.Value;
					}
					else
					{
						parentState.score = Mathf.Min(parentState.score.Value, newState.score.Value);
						parentState.alpha= Mathf.Min(parentState.alpha, newState.score.Value);
					}
					
					if (parentState.score.Value > parentState.beta)
					{
						Debug.Log("MINMAXPRUNING --");

						return nextStates;
					}
				}
			}
		}

		return nextStates;
	}
}

public class State
{
	public Piece movedPiece;

	public List<Piece> pieces;

	public List<State> nextStates;

	public State parent;

	public int? score = null;

	public int alpha = int.MinValue;

	public int beta = int.MaxValue;

	public State()
	{
		pieces = new List<Piece>();
		nextStates = new List<State>();
	}

	public void Evaluate()
	{
		score = 0;
		foreach (Piece piece in pieces)
		{
			if (piece.Color == AiManager.AiColor) score += piece.Value;
			else score -= piece.Value;
		}
	}
}