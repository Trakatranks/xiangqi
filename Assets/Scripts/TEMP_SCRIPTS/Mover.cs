﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
	public float space = 2.0f;

	public Vector3 startPos;

	private void Start()
	{
		startPos = transform.position;
	}

	// Update is called once per frame
	void Update()
    {
		transform.position = startPos + new Vector3(Mathf.Cos(Time.time) * space, 0, Mathf.Sin(Time.time) * space);
	}
}
