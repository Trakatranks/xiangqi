﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardEditor : MonoBehaviour
{
	protected BoardData currentBoardData;

	[SerializeField]
	protected bool isTriggerBrush = false;
	[SerializeField]
	protected bool isWalkablebrush = false;

	[Space()]

	[Tooltip("Must be in this order : PAWN, CANNON, TOWER, HORSE, ELEPHANT, MINISTER, KING, NULL")]
	public Sprite[] piecesSprites;

	[Space()]
	protected PieceType currentBrushPiece;
	protected PieceColor currentBrushColor;
	protected int brushTriggerID = 0;
	
	[SerializeField]
	protected float cellOffset = .5f;

	[SerializeField]
	protected Camera editorCamera;
	[SerializeField]
	protected float camSpeed;

	[SerializeField]
	protected Transform gridContainer;
	[SerializeField]
	protected EditorCell EditorCellPrefab;

	//GUI variables
	[Header("GUI")]
	public float guiOffset = 10.0f;
	public float toolSize = 2.0f;
	float btnH = 30.0f;
	string xInput = "";
	string yInput = "";

	int pieceColorSelector = 0;
	string[] colorChoice = { "White", "Black" };

	int pieceTypeSelector = 0;
	string[] typeChoice = { "Pawn", "Cannon", "Tower", "horse", "Elephant", "Minister", "King", "None" };

	string fileName = "";

	private void OnGUI()
	{
		float w = Screen.width / 4 * toolSize;
		float elemXPos = guiOffset * 2;

		Rect boxRect = new Rect(guiOffset, guiOffset, w, Screen.height - guiOffset * 2);
		GUI.Box(boxRect, "Tools");

		float sizeInputsWidth = (w - guiOffset * 4) / 3;

		xInput = GUI.TextField(new Rect(elemXPos, 50, sizeInputsWidth, btnH), xInput);
		yInput = GUI.TextField(new Rect(elemXPos + sizeInputsWidth + guiOffset, 50, sizeInputsWidth, btnH), yInput);
		bool shouldGenerateBoard = GUI.Button(new Rect(elemXPos + sizeInputsWidth * 2 + guiOffset*2, 50, sizeInputsWidth, btnH), "Create");

		pieceColorSelector = GUI.Toolbar(new Rect(elemXPos, 90, w-guiOffset*2, btnH), pieceColorSelector, colorChoice);
		currentBrushColor = pieceColorSelector == 0 ? PieceColor.WHITE : PieceColor.BLACK;

		pieceTypeSelector = GUI.SelectionGrid(new Rect(elemXPos, 130, w - guiOffset * 2, btnH * 8 + 70), pieceTypeSelector, typeChoice, 1);
		currentBrushPiece = (PieceType)pieceTypeSelector;

		brushTriggerID = (int)Mathf.Round(GUI.HorizontalSlider(new Rect(elemXPos, 450, w / 2, btnH), brushTriggerID ,0 , 10));
		GUI.Label(new Rect(elemXPos + w / 2 + guiOffset, 445, 30, btnH), "" + brushTriggerID);

		isTriggerBrush = GUI.Toggle(new Rect(elemXPos, 465, w - guiOffset / 2, btnH), isTriggerBrush, " isTrigger");
		isWalkablebrush = GUI.Toggle(new Rect(elemXPos, 485, w / 2 - guiOffset / 2, btnH), isWalkablebrush, " isWalkable");

		fileName = GUI.TextField(new Rect(elemXPos, 515, w-guiOffset*2, btnH), fileName);

		bool shouldLoad = GUI.Button(new Rect(elemXPos, 555, (w - guiOffset * 3) / 2, btnH), "Load Map");
		bool shouldSave = GUI.Button(new Rect(elemXPos + (w - guiOffset * 3) / 2 + guiOffset, 555, (w - guiOffset * 3) / 2, btnH), "Save Map");

		if (shouldGenerateBoard) SetSizeButtonAction();
		if (shouldSave) SaveButtonAction();
		else if(shouldLoad) LoadButtonAction();
	}

	private void Update()
	{
		if (Input.GetKey(KeyCode.UpArrow)) editorCamera.transform.position += Vector3.forward * camSpeed;
		else if (Input.GetKey(KeyCode.DownArrow)) editorCamera.gameObject.transform.position -= Vector3.forward * camSpeed;
		if (Input.GetKey(KeyCode.RightArrow)) editorCamera.transform.position += Vector3.right * camSpeed;
		else if (Input.GetKey(KeyCode.LeftArrow)) editorCamera.gameObject.transform.position -= Vector3.right * camSpeed;

		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = editorCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out RaycastHit hitInfo))
			{
				Debug.DrawRay(ray.origin, ray.direction, Color.red, 5.0f);
				if (hitInfo.collider.gameObject.GetComponent<EditorCell>() != null)
				{
					EditorCell cell = hitInfo.collider.gameObject.GetComponent<EditorCell>();
					SetCellValues(cell);
				}
			}
		}
	}

	public void SetCellValues(EditorCell cell)
	{
		if (isWalkablebrush || isTriggerBrush)
		{
			if (isTriggerBrush)
			{
				cell.cellData.trigger.ID = brushTriggerID;
				cell.SetIdText(brushTriggerID);
			}

			if (isWalkablebrush)
			{
				cell.cellData.isWalkable = !cell.cellData.isWalkable;
				cell.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", cell.cellData.isWalkable ? Color.green : Color.red);
			}

		}
		else 
		{
			if(cell.cellData.pieceData.pieceType != currentBrushPiece || cell.cellData.pieceData.playerID != (currentBrushColor == PieceColor.BLACK? 1 : 0))
			{
				cell.cellData.pieceData.pieceType = currentBrushPiece;
				cell.cellData.pieceData.playerID = currentBrushColor == PieceColor.BLACK ? 1 : 0;
				cell.SetIcon(piecesSprites[(int)currentBrushPiece]);
				cell.SetIconColor(currentBrushColor);
			}
			else
			{
				cell.cellData.pieceData.pieceType = PieceType.NONE;
				cell.cellData.pieceData.playerID = default;
				cell.SetIcon(piecesSprites[7]);
			}
		}
		currentBoardData.cells[cell.cellData.gridPosX + (cell.cellData.gridPosY * currentBoardData.boardSizeX)] = cell.cellData;
	}

	public void EraseGrid()
	{
		int nbChilds = gridContainer.childCount;
		for (int i = 0; i < nbChilds; i++)
		{
			Destroy(gridContainer.GetChild(i).gameObject);
		}
	}

	public void DrawBoard(BoardData board, bool fromLoad)
	{
		if (gridContainer.childCount > 0) EraseGrid();

		for (int i = 0; i < board.boardSizeX; i++)
		{
			for (int j = 0; j < board.boardSizeY; j++)
			{
				GameObject currentGO = Instantiate(EditorCellPrefab.gameObject, new Vector3(i + i*cellOffset, 0, j + j*cellOffset), Quaternion.identity, gridContainer);
				EditorCell currentEC = currentGO.GetComponent<EditorCell>();
				MeshRenderer currentMR = currentGO.GetComponent<MeshRenderer>();

				if (fromLoad)
				{
					currentEC.cellData = board.cells[i + (j * board.boardSizeX)];
				}
				else
				{
					board.cells[i + (j * board.boardSizeX)] = new CellData();
				}

				currentEC.SetCellGridPos(i, j);

				currentBoardData.cells[i + (j * currentBoardData.boardSizeX)].gridPosX = i;
				currentBoardData.cells[i + (j * currentBoardData.boardSizeX)].gridPosX = j;

				currentMR.material.SetColor("_BaseColor", currentEC.cellData.isWalkable ? Color.green : Color.red);

				//Debug.Log(currentEC.cellData.gridPosX + " || " + currentEC.cellData.gridPosY + " : " + currentEC.cellData.pieceData.pieceType);

				currentEC.SetIcon(piecesSprites[(int)currentEC.cellData.pieceData.pieceType]);
				currentEC.SetIconColor(currentEC.cellData.pieceData.playerID == 1 ? PieceColor.BLACK : PieceColor.WHITE);

				currentEC.SetIdText(currentEC.cellData.trigger.ID);
			}
		}

		editorCamera.transform.position = new Vector3(currentBoardData.boardSizeX / 2, 10, currentBoardData.boardSizeY / 2);
		Debug.Log("XXX --- BOARD DRAWN --- XXX");
	}
	
	public void SetSizeButtonAction()
	{
		int x = int.TryParse(xInput, out int xresult) ? int.Parse(xInput) : 9;
		int y = int.TryParse(xInput, out int yresult) ? int.Parse(yInput) : 10;
		currentBoardData = new BoardData(x, y);
		if (x > 0 && y > 0)
			DrawBoard(currentBoardData, false);
	}

	public void SaveButtonAction()
	{
		for (int i = 0; i < currentBoardData.boardSizeX; i++)
		{
			for (int j = 0; j < currentBoardData.boardSizeY; j++)
			{
				Debug.Log("i:" + i + " || j:" + j + " || index:" + (i + (j * currentBoardData.boardSizeX)) + " || walk:" + currentBoardData.cells[i + (j * currentBoardData.boardSizeX)].isWalkable);
			}
		}

		Debug.Log(currentBoardData);
		string saveName = fileName;
		BoardSaver.SaveBoard(currentBoardData,saveName);
	}

	public void LoadButtonAction()
	{
		currentBoardData = BoardSaver.LoadBoard(fileName);
		foreach (CellData cd in currentBoardData.cells)
		{
			Debug.Log(cd.gridPosX + " " + cd.gridPosY + " " + cd.pieceData.pieceType);
		}
		DrawBoard(currentBoardData, true);
	}
}
