﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class BoardSaver
{
	protected static string _filePath = Application.streamingAssetsPath + Path.DirectorySeparatorChar + "maps" + Path.DirectorySeparatorChar;
	protected static string _fileExtention = ".xqmap";

	public static void SaveBoard(BoardData boardData, string boardName)
	{
		boardName = boardName.ToLower();

		BinaryFormatter formatter = new BinaryFormatter();
		FileStream stream = new FileStream(_filePath + boardName + _fileExtention, FileMode.Create);

		formatter.Serialize(stream, boardData);
		stream.Close();
		Debug.Log("SAVED UNDER : " + _filePath + boardName + _fileExtention);
	}

	public static BoardData LoadBoard(string boardName)
	{
		BinaryFormatter formatter = new BinaryFormatter();
		FileStream stream = new FileStream(_filePath + boardName + _fileExtention, FileMode.Open);

		BoardData bd = formatter.Deserialize(stream) as BoardData;
		stream.Close();
		return bd;
	}
}
