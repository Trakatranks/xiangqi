﻿using UnityEngine;
using UnityEngine.UI;

public class EditorCell : MonoBehaviour
{
	[SerializeField]
	protected SpriteRenderer icon;
	[SerializeField]
	protected Text IdText;
	public CellData cellData;

	private void Awake()
	{
		cellData = new CellData();
	}

	public void SetCellGridPos(int x, int y)
	{
		cellData.gridPosX = x;
		cellData.gridPosY = y;
	}

	public void SetIcon(Sprite pieceIcon)
	{
		icon.sprite = pieceIcon;
	}

	public void SetIconColor(PieceColor pieceColor)
	{
		switch (pieceColor)
		{
			case PieceColor.BLACK:
				icon.color = Color.black;
				break;
			case PieceColor.WHITE:
				icon.color = Color.red;
				break;
			case PieceColor.ANY:
				icon.color = Color.red;
				break;
			default:
				icon.color = Color.red;
				break;
		}
	}

	public void SetIdText(int ID)
	{
		IdText.text = "" + ID;
	}
}
